from django import forms
from ratings.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ['brewery_name', 'beer_name', 'score', 'notes']
