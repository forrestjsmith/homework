from django.db import models
from django.urls import reverse


class Rating(models.Model):
    brewery_name = models.CharField(max_length=200)
    beer_name = models.CharField(max_length=200)
    score = models.DecimalField(max_digits=3, decimal_places=1)
    notes = models.TextField(blank=True)
    class Meta:
        ordering = ['brewery_name', 'beer_name', '-score']
    def get_absolute_url(self):
        return reverse('rating-home')
