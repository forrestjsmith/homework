const newRatingForm = document.getElementById('ratingForm');
const button = document.getElementById('newRatingToggle');

button ? button.addEventListener('click', toggleForm) : null;

function toggleForm() {
   newRatingForm.classList.toggle('show');
}