from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.views.generic.edit import UpdateView, DeleteView

from ratings.models import Rating
from ratings.forms import RatingForm


def home(request):
    """ Show the entry point to the ratings app
    :param request: Django request object
    :return: rendered homepage
    """
    context = {'ratings': Rating.objects.all()}
    return render(request, 'home.html', context)

def createRating(request):
    form = RatingForm(request.POST)
    """ Capitalize first letter of every word for breweries and beers """
    new_rating = form.save(commit=False)
    new_rating.brewery_name = new_rating.brewery_name.title()
    new_rating.beer_name =  new_rating.beer_name.title()
    if form.is_valid():
        new_rating.save()
        return redirect('/')

class RatingUpdate(UpdateView):
    """ Edit existing rating """
    model = Rating
    fields = ['brewery_name', 'beer_name', 'score', 'notes']


class RatingDelete(DeleteView):
    """ Delete rating """
    model = Rating
    success_url = '/'